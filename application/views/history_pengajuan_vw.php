<style type="text/css">
    hr{

        border:0;
        border-top:3px double #8c8c8c;
    }
</style>
<script src="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker.js"></script>





    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>History Pengajuan Request</h2>

        </div>

    </div>

    <div class="wrapper wrapper-content  animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <div class="tab-content">
                        <div class="form-group">
                            <label>Pilih Tanggal Laporan :</label>
                            <form method="get" action="<?php echo base_url();?>laporan/pendapatan_per_cs">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input class="form-control" type="text" name="daterange" placeholder="Pilih Tanggal">
                                    <span class="input-group-btn">
                      <input type="submit" class="btn btn-info btn-flat" value='Go!'>
                    </span>
                                </div>
                            </form>
                            <br>


                        </div></div>
                </div>
                <div class="col-sm-5">

                    <div class="ibox-content">

                        <div class="clients-list">

                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="full-height-scroll">
                                        <div class="table-responsive">
                                            <hr>
                                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                                <thead>
                                                <tr>
                                                    <th width='5%' style="text-align:center;background:#AFEEEE;">No</th>
                                                    <th style="background:#AFEEEE">Tanggal</th>
                                                    <th style="background:#AFEEEE">No Pengajuan</th>
                                                    <th style="text-align:center;background:#AFEEEE">View</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $no=1;
                                                foreach($history as $ad): ?>
                                                <tr>
                                                    <td><?php echo $no++; ?></td>
                                                   <td><?php echo $ad->tanggal_pengajuan; ?></td>
                                                    <td><?php echo $ad->no_pengajuan; ?></td>
                                                    <td class="client-status" align="center"><span class="label label-danger"><a data-toggle="tab"  id="<?php echo $ad->id_pengajuan; ?>" data-awal="<?php echo $awal; ?>" data-akhir="<?php echo $akhir; ?>" class="client-link" onClick="reply_click(this,this,this.id)">Detail</a></span></td>
                                                </tr>
                                                <?php endforeach; ?>

                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-sm-7">
                    <div class="ibox-content">
                        <div class="tab-content">
                            <div id="contact-1" class="tab-pane active">

                                <div class="client-detail">
                                    <div class="full-height-scroll tampildata" >


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="footer">
                <div>
                    <strong>Copyright</strong> Tim Online Bursasajadah &copy; 2017
                </div>
            </div>
            <!-- Mainly scripts -->


            <!-- Custom and plugin javascript -->

            <script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
            <script src="<?php echo base_url();?>assets/js/plugins/fullcalendar/moment.min.js"></script>
            <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

            <script type="text/javascript">
                function reply_click(obj,obj,clicked_id)
                {

                    var diskusi_id=clicked_id
                    var product_id = obj.getAttribute('data-awal');
                    var product_id2 = obj.getAttribute('data-akhir');
                    $('.tampildata').load("<?php echo base_url() ?>Pengajuan/replay/"+diskusi_id+"/"+product_id+"/"+product_id2);


                }


                $(function() {
                    $('input[name="daterange"]').daterangepicker({
                        format: 'YYYY/MM/DD'
                    });
                });



                $('#edit_data-btn').on("click",function(e){
                    id_array= new Array();
                    i=0;
                    $("input.chk:checked").each(function(){
                        id_array[i] = $(this).val();
                        i++;

                    })
                    if(id_array.length<1){
                        alert('Pilih data yang akan didownload terlebih dahulu');
                        return false;

                    }
                });// en
            </script>
