<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */



    function __construct()
    {
        parent::__construct();
        $this->load->model('Pengajuan_model');
        $this->load->library('cart');
        $this->load->library('session');
    }

	public function index()
	{
        $this->load->view('layouts/header');
	    $this->load->view('dashboard');
        $this->load->view('layouts/footer');
	}

    public function tambah_pengajuan()
    {
        $data['kodeunik'] = $this->Pengajuan_model->code_otomatis();
        $data['jenis_request']=$this->Pengajuan_model->select_jenis_request();
        $data['barang']=$this->Pengajuan_model->select_master_barang();
        $this->load->view('layouts/header');
        $this->load->view('tambah_pengajuan_vw',$data);
        $this->load->view('layouts/footer');
    }


    function getharga($id){

        $result = $this->Pengajuan_model->get_harga($id);
        $arr = $result->row_array();
        //var_dump($result->row_array());die;

        $data['harga'] = isset($arr['harga'])?$arr['harga']:0; // ini ngambil ke master mst_produk
        echo json_encode($data);
    }

    function add(){
        $barang=$this->input->post('id_barang');

        $pisahbarang = explode("|",$barang);
        $id_barang   = ($pisahbarang[0]);
        $nama_barang = ($pisahbarang[1]);


        $data=array(
            'id'=>$id_barang,
            'name'=>$nama_barang,
            'qty'=>$this->input->post('qty'),
            'price'=>$this->input->post('harga'),
            'keterangan'=>$this->input->post('keterangan')

        );
        $this->cart->insert($data);
        redirect('Pengajuan/tambah_pengajuan');
    }


    function hapus_pengajuan($product){
        $data = array(
            'rowid' => $product,
            'qty' => 0
        );

        $this->cart->update($data);
        redirect('Pengajuan/tambah_pengajuan');
    }

    public function save_pengajuan(){

        $ID = $this->ion_auth->user()->row()->id;
        $no_pengajuan              = $this->input->post('no_pengajuan');
        $tanggal_pengajuan         = $this->input->post('tanggal_pengajuan');
        $prioritas                 = $this->input->post('prioritas');
        $id_jenis_request          = $this->input->post('id_jenis_request');
        $keterangan                = $this->input->post('keterangan');

        $this->Pengajuan_model->insert_pengajuan($ID,$tanggal_pengajuan,$prioritas,$id_jenis_request,$keterangan,$no_pengajuan);
        $last_insert_id = $this->db->insert_id();

        foreach ($this->cart->contents() as $items):

            $order_detail['id_pengajuan'] =  $last_insert_id;
            $order_detail['id_barang'] = $items['id'];
            $order_detail['qty'] = $items['qty'];
            $order_detail['harga'] = $items['price'];

            $order_detail['tharga'] =  $items['subtotal'];
            $order_detail['keterangan'] =  $items['keterangan'];
            $this->db->insert("detail_pengajuan",$order_detail);
        endforeach;


        $this->cart->destroy();
        $this->session->unset_userdata('users');
        redirect('Pengajuan/tambah_pengajuan');
    }


    public function history_pengajuan()
    {
        $ID = $this->ion_auth->user()->row()->id;
        $data['history']=$this->Pengajuan_model->select_pengajuan_id($ID);

        $this->load->view('layouts/header');
        $this->load->view('history_pengajuan_vw',$data);
        $this->load->view('layouts/footer');
    }

    function replay($diskusi_id,$product_id,$product_id2){
        $data['replay'] = $this->Pengajuan_model->get_kode($diskusi_id,$product_id,$product_id2);
        $repsan= $this->Pengajuan_model->get_kode($diskusi_id,$product_id,$product_id2);


        foreach($repsan->result() as $an){
            $id_pengajuan=$an->id_pengajuan;
            $data['id_pengajuan']=$an->id_pengajuan;
            $data['no_pengajuan']=$an->no_pengajuan;
            $data['tanggal_pengajuan']=$an->tanggal_pengajuan;
            $data['prioritas']=$an->prioritas;
            $data['jenis_request']=$an->jenis_request;
            $data['keterangan']=$an->keterangan;
            $data['last_status']=$an->last_status;
        }


        echo $id_pengajuan;
        die;

        $data['user_id']=$diskusi_id;
        $data['awal']=$product_id;
        $data['akhir']=$product_id2;

        $this->load->view('replay',$data);
    }




}
