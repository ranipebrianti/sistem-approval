
<?php
Class Pengajuan_model extends CI_Model
{


//=============================================================================================================== select


    //========================================= lokasi

    function select_master_barang(){
        $this->db->select('*');
        $this->db->from('barang_detail');
        return $this->db->get()->result();
    }


    function select_jenis_request(){
        $this->db->select('*');
        $this->db->from('jenis_request');
        return $this->db->get()->result();
    }

    function get_harga($id){
        $this->db->select('harga');
        $this->db->from('barang_detail');
        $this->db->where('id_barang_detail',$id);
        return $this->db->get();
    }


    function insert_pengajuan($ID,$tanggal_pengajuan,$prioritas,$id_jenis_request,$keterangan,$no_pengajuan){

        $this->db->set('id_user', $ID);
        $this->db->set('tanggal_pengajuan', $tanggal_pengajuan);
        $this->db->set('prioritas', $prioritas);
        $this->db->set('id_jenis_request', $id_jenis_request);
        $this->db->set('keterangan', $keterangan);
        $this->db->set('no_pengajuan', $no_pengajuan);
        $this->db->set('last_status', 0);
        $this->db->insert('pengajuan');
    }

    function select_pengajuan_id($ID){
        $this->db->select('*');
        $this->db->from('pengajuan');
        $this->db->where('id_user',$ID);
        $this->db->order_by('tanggal_pengajuan','DESC');
        return $this->db->get()->result();
    }


    function get_kode($user_id,$awal,$akhir){

        $this->db->select('*');
        $this->db->from('pengajuan');
        $this->db->join('jenis_request','pengajuan.id_jenis_request = jenis_request.id_jenis_request');
        $this->db->join('detail_pengajuan','pengajuan.id_pengajuan = detail_pengajuan.id_pengajuan');
        $this->db->where('pengajuan.id_pengajuan',$user_id);
        $this->db->group_by('detail_pengajuan.id_pengajuan');

        return $this->db->get();

    }


    function code_otomatis(){
        $this->db->select('Right(pengajuan.no_pengajuan,4) as kode ',false);
        $this->db->order_by('id_pengajuan', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('pengajuan');
        if($query->num_rows()<>0){
            $data = $query->row();
            $kode = intval($data->kode)+1;
        }else{
            $kode = 1;

        }
        $kodemax = str_pad($kode,4,"0",STR_PAD_LEFT);
        $kodejadi  = "PR-".$kodemax;
        return $kodejadi;

    }


}